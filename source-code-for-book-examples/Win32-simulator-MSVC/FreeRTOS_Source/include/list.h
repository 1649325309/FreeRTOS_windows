/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    
	http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。
*/

/*
 * 这是调度器使用的链表的实现。按照调度器的需求进行了精心的裁剪，也可以用于应
 * 用代码中。
 * 
 * 链接只存储指向链表项的指针。每个ListItem_t包含一个数字值(xItemValue).大多数
 * 情况链表是按每项值的降序排列的。
 *
 * 列表在创建时就已经包含一个列表项。这项的值是表示列表最大可存储的值，因此
 * 它总是在列表尾，并用作为一个标记。列表的pxHead总是指向这个标记--即使它是
 * 处于列表尾。这是因为尾项包一个向后指向真正列表头的指针。
 * 
 * 除了它的值，每个列表项包含一个指针指向列表的下一项(pxNext),一个指针指向
 * 它自己所在的列表(pxContainer),一个向后指向包含它的对象的指针。其中后面的两个
 * 指针是为了增加对列表的操作效率。这是在包含列表项的对象与列表项自己之间两种
 * 高效的链接方式
 */

#ifndef INC_FREERTOS_H
	#error FreeRTOS.h must be included before list.h
#endif

#ifndef LIST_H
#define LIST_H

/*
 * 列表结构成员是在中断中修改的。因此按理应声明为volatile.但是，它们都在一个
 * 函数中以原子操作修改的(在调度器挂起的临界区)，并且要么通过把引用传入到函数，
 * 要么通过一个volatile变量的索引传入到函数。在所有目前使用的情况中，volatile限
 * 定符可以省略，以适度的提高性能，前提是不影响函数功能。
 * IAR、ARM和GCC编译器在各自的编译器选项设置为实现最大优化时生成的汇编指令
 * 已被检查并认为符合预期。这就是说，随着编译器技术的进步，
 * 特别是如果使用了激进的跨模块优化（一个还没有得到很大程度应用的用例），
 * 那么需要volatile限定符来进行正确的优化是可行的。由于没有列表结构成员上的volatile限定符
 * 并且进行激进的跨模块优化，编译器认为不必要的代码将导致调度器的完全和明显的故障，
 * 如果遇到这种情况，那么volatile限定符可以插入到列表结构中的相关位置，
 * 只需在FreeRTOSConfig.h中定义configLIST_volatile to volatile(像本注释块后面的示例那样)
 * 
 * 如果没有定义configLIST_VOLATILE，下面的预处理指令会把它定义为无
 * #define configLIST_VOLATILE
 *
 * 要把列表项的每一项添加volatile,只需在FreeRTOSConfig.h中添加如下定义(去掉引号)
 * "#define configLIST_VOLATILE volatile" 
 */
#ifndef configLIST_VOLATILE
	#define configLIST_VOLATILE
#endif /* configSUPPORT_CROSS_MODULE_OPTIMISATION */

#ifdef __cplusplus
extern "C" {
#endif

/* 这些宏用于在链表结构中放置已知的值，然后检测这些给定的值在应用执行的过程中是否
被破坏。这样就可以检测出数据结构在内存中是否被破坏。这些宏不能捕获由于错误的配置或
错误使用FreeRTOS导致的数据错误.*/
#if( configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES == 0 )
	/* 把宏定义为什么也不做 */
	#define listFIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE
	#define listSECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE
	#define listFIRST_LIST_INTEGRITY_CHECK_VALUE
	#define listSECOND_LIST_INTEGRITY_CHECK_VALUE
	#define listSET_FIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem )
	#define listSET_SECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem )
	#define listSET_LIST_INTEGRITY_CHECK_1_VALUE( pxList )
	#define listSET_LIST_INTEGRITY_CHECK_2_VALUE( pxList )
	#define listTEST_LIST_ITEM_INTEGRITY( pxItem )
	#define listTEST_LIST_INTEGRITY( pxList )
#else
	/* 把宏定义为添加新的成员到列表结构中. */
	#define listFIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE				TickType_t xListItemIntegrityValue1;
	#define listSECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE				TickType_t xListItemIntegrityValue2;
	#define listFIRST_LIST_INTEGRITY_CHECK_VALUE					TickType_t xListIntegrityValue1;
	#define listSECOND_LIST_INTEGRITY_CHECK_VALUE					TickType_t xListIntegrityValue2;

	/* 把宏定义为将新的结构成员赋为一个已知值. */
	#define listSET_FIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem )		( pxItem )->xListItemIntegrityValue1 = pdINTEGRITY_CHECK_VALUE
	#define listSET_SECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE( pxItem )	( pxItem )->xListItemIntegrityValue2 = pdINTEGRITY_CHECK_VALUE
	#define listSET_LIST_INTEGRITY_CHECK_1_VALUE( pxList )		( pxList )->xListIntegrityValue1 = pdINTEGRITY_CHECK_VALUE
	#define listSET_LIST_INTEGRITY_CHECK_2_VALUE( pxList )		( pxList )->xListIntegrityValue2 = pdINTEGRITY_CHECK_VALUE

	/* 把宏定义为一个断言，检测一个结构成员的值是否与它其望包含的值一致. */
	#define listTEST_LIST_ITEM_INTEGRITY( pxItem )		configASSERT( ( ( pxItem )->xListItemIntegrityValue1 == pdINTEGRITY_CHECK_VALUE ) && ( ( pxItem )->xListItemIntegrityValue2 == pdINTEGRITY_CHECK_VALUE ) )
	#define listTEST_LIST_INTEGRITY( pxList )			configASSERT( ( ( pxList )->xListIntegrityValue1 == pdINTEGRITY_CHECK_VALUE ) && ( ( pxList )->xListIntegrityValue2 == pdINTEGRITY_CHECK_VALUE ) )
#endif /* configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES */


/*
 * 定义列表可以包含的对象的类型的.
 */
struct xLIST_ITEM
{
	listFIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE			/*< 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 设为 1，设为一个已知值. */
	configLIST_VOLATILE TickType_t xItemValue;			/*< 列的值，大多情况下，用于对列表降序排序. */
	struct xLIST_ITEM * configLIST_VOLATILE pxNext;		/*< 指向列表中下一项. */
	struct xLIST_ITEM * configLIST_VOLATILE pxPrevious;	/*< 指向列表的前一项. */
	void * pvOwner;										/*< 指向包含列表项的对象(一般是一个TCB)，因此在包含列表项的对象与列表项本身之前有两条链接方式. */
	void * configLIST_VOLATILE pvContainer;				/*< 指向放置列表项的列表(if any). */
	listSECOND_LIST_ITEM_INTEGRITY_CHECK_VALUE			/*< 如果 configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 设为1.设置一个已知值 */
};
typedef struct xLIST_ITEM ListItem_t;					/* lint检测需要把这两个定义分开. */

struct xMINI_LIST_ITEM
{
	listFIRST_LIST_ITEM_INTEGRITY_CHECK_VALUE			/*< 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 设为 1，设为一个已知值. */
	configLIST_VOLATILE TickType_t xItemValue;
	struct xLIST_ITEM * configLIST_VOLATILE pxNext;
	struct xLIST_ITEM * configLIST_VOLATILE pxPrevious;
};
typedef struct xMINI_LIST_ITEM MiniListItem_t;

/*
 * 定义调度器使用的队列类型.
 */
typedef struct xLIST
{
	listFIRST_LIST_INTEGRITY_CHECK_VALUE				/*< 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 设为 1，设为一个已知值. */
	configLIST_VOLATILE UBaseType_t uxNumberOfItems;
	ListItem_t * configLIST_VOLATILE pxIndex;			/*< 用于遍历链表.  Points to the last item returned by a call to listGET_OWNER_OF_NEXT_ENTRY (). */
	MiniListItem_t xListEnd;							/*< 包含最大可能项值的列表项，这意味着它始终位于列表的末尾，因此用作标记. */
	listSECOND_LIST_INTEGRITY_CHECK_VALUE				/*< 如果configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 设为 1，设为一个已知值. */
} List_t;

/*
 * 一个访问宏，设置列表项的捅有者。列表项的捅有者是一个包含列表项的对象(通常是TCB)
 */
#define listSET_LIST_ITEM_OWNER( pxListItem, pxOwner )		( ( pxListItem )->pvOwner = ( void * ) ( pxOwner ) )

/*
 * 一个访问宏，获取列表项的捅有者。列表项的捅有者是一个包含列表项的对象(通常是TCB)
 *
 * \page listSET_LIST_ITEM_OWNER listSET_LIST_ITEM_OWNER
 * \ingroup LinkedList
 */
#define listGET_LIST_ITEM_OWNER( pxListItem )	( ( pxListItem )->pvOwner )

/*
 * 一个访问宏，设置列表项的值.大多数情况下这个值用于对列表降序排序
 *
 */
#define listSET_LIST_ITEM_VALUE( pxListItem, xValue )	( ( pxListItem )->xItemValue = ( xValue ) )

/*
 * 访问宏，用于获取列表项的值。这个值可以表示任何意义--比如任务优先级，
 * 或者解除阻塞的时间值 
 */
#define listGET_LIST_ITEM_VALUE( pxListItem )	( ( pxListItem )->xItemValue )

/*
 * 访问宏，获取列表第一项的值
 */
#define listGET_ITEM_VALUE_OF_HEAD_ENTRY( pxList )	( ( ( pxList )->xListEnd ).pxNext->xItemValue )

/*
 * 获取列表头.
 */
#define listGET_HEAD_ENTRY( pxList )	( ( ( pxList )->xListEnd ).pxNext )

/*
 * 获取下一项
 */
#define listGET_NEXT( pxListItem )	( ( pxListItem )->pxNext )

/*
 * 返回列表中包含列表尾标记的列表项
 */
#define listGET_END_MARKER( pxList )	( ( ListItem_t const * ) ( &( ( pxList )->xListEnd ) ) )

/*
 * 检测列表中是否包含列表项，只有列表为空时才返回true
 */
#define listLIST_IS_EMPTY( pxList )	( ( BaseType_t ) ( ( pxList )->uxNumberOfItems == ( UBaseType_t ) 0 ) )

/*
 * 访问宏，获取列表中的项数
 */
#define listCURRENT_LIST_LENGTH( pxList )	( ( pxList )->uxNumberOfItems )

/*
 * 获取列表中下一项的所有者的访问函数.
 *
 * 链表中pxIndex成员是用来对链表进行遍历的.调用listGET_OWNER_OF_NEXT_ENTRY将会增加
 * pxIndex，使其指向下一项，并返回下一项的捅有者pxOwner。多次调用这个函数，就可以遍历链表
 * 包含的每一项Calling。
 *
 * 链表中pxOwner指向链表的捅有者。在调度器中，这个一般是控制块
 * 这个pxOwner高效的在列表项和它捅有者之间建立起两个边接通道
 *
 * @param pxTCB pxTCB 为下一项捅有者的地址.
 * @param pxList 返回下一项捅有者的链表.
 */
#define listGET_OWNER_OF_NEXT_ENTRY( pxTCB, pxList )										\
{																							\
List_t * const pxConstList = ( pxList );													\
	/* 增加索引指向下一项，返回它。确保我们没有把链表尾标记返回了 */														\
	( pxConstList )->pxIndex = ( pxConstList )->pxIndex->pxNext;							\
	if( ( void * ) ( pxConstList )->pxIndex == ( void * ) &( ( pxConstList )->xListEnd ) )	\
	{																						\
		( pxConstList )->pxIndex = ( pxConstList )->pxIndex->pxNext;						\
	}																						\
	( pxTCB ) = ( pxConstList )->pxIndex->pvOwner;											\
}


/*
 * 获取列表中第一条目的所有者的访问函数.  
 * 列表通常按项目值的升序排序。
 * 
 * 该函数返回列表的第一项的pxOwner成员。pxOwner是一个指向捅有该列表的指针。
 * 在调度器中，通常是任务控制块。
 * pxOwner参数有效地在列表项和它的所有者之间创建一个双向链接。
 *
 * @param pxList 需要返回该链表的捅有者
 *
 */
#define listGET_OWNER_OF_HEAD_ENTRY( pxList )  ( (&( ( pxList )->xListEnd ))->pxNext->pvOwner )

/*
 * 检查列表项是否在指定的列表中。列表项维护着一个"container"指针，指向列表项所在的链表。
 * 该宏就是检测它的container是否与list匹配。
 *
 * @param pxList 检查列表项是否在这个列表中.
 * @param pxListItem 要检查的列表项.
 * @return  匹配返回pdTRUE, 否则返回 pdFALSE.
 */
#define listIS_CONTAINED_WITHIN( pxList, pxListItem ) ( ( BaseType_t ) ( ( pxListItem )->pvContainer == ( void * ) ( pxList ) ) )

/*
 * 返回列表项所在的列表 (是一个引用).
 *
 * @param pxListItem 要查询的列表项.
 * @return 指向引用pxListItem的列表对象的指针
 */
#define listLIST_ITEM_CONTAINER( pxListItem ) ( ( pxListItem )->pvContainer )

/*
 * 这提供了一种粗略的方法来了解列表是否已初始化, 因为在vListInitialise()中有一个赋值
 * pxList->xListEnd.xItemValue 被赋值为 portMAX_DELAY 
 */
#define listLIST_IS_INITIALISED( pxList ) ( ( pxList )->xListEnd.xItemValue == portMAX_DELAY )

/*
 * 必须在使用列表之前调用！这将初始化列表结构的所有成员，
 * 并将xListEnd项作为列表结束的标记插入到列表中。
 *
 * @param pxList 指向要初始化的列表.
 */
void vListInitialise( List_t * const pxList ) PRIVILEGED_FUNCTION;

/*
 * 在列表项使用之前必须先初始化.这个调用会会把container赋为null。
 * 这样使得该列表项不处于任何列表中
 *
 * @param pxItem 指向要初始化的列表项.
 *
 */
void vListInitialiseItem( ListItem_t * const pxItem ) PRIVILEGED_FUNCTION;

/*
 * 插入一个列表项到列表. 该项在列表中的插入位置由它的值决定.
 *
 * @param pxList 该项要插入的列表.
 *
 * @param pxNewListItem 要插入列表的项.
 */
void vListInsert( List_t * const pxList, ListItem_t * const pxNewListItem ) PRIVILEGED_FUNCTION;

/*
 * 插入一项列表中。该项插入的位置，是多次调用listGET_OWNER_OF_NEXT_ENTRY返回的最后一个
 * 译注(即列表尾标记的前一项，也就是使用listGET_OWNER_OF_NEXT_ENTRY遍历的最后一个返回值)
 * pvIndex是用于对列表进行遍历的. 调用listGET_OWNER_OF_NEXT_ENTRY会增加pvIndex，使期指向
 * 列表中的下一项。使用vListInsertEnd将项放入列表中，
 * 可以高效的将该项放置在pvIndex指向的列表位置。这也就意味着列表中的其他项，
 * 在调用listGET_OWNER_OF_NEXT_ENTRY返回时，要先于pvIndex再次指向的插入项之前返回。
 * 译注(这里弄了一大堆注释，其实就是说使用pvIndex，快速的将新项插入到了列表尾,
 * 刚插入的项，也是使用listGET_OWNER_OF_NEXT_ENTRY遍历时返回的最后一项)
 *
 * @param pxList 该项要插入的列表.
 *
 * @param pxNewListItem 插入列表中的项.
 *
 */
void vListInsertEnd( List_t * const pxList, ListItem_t * const pxNewListItem ) PRIVILEGED_FUNCTION;

/*
 * 从列表中移除一项。列表向有一个指向它自己所在列表的指针，所以只需要把列表
 * 项传入函数即可
 *
 * @param uxListRemove 要移除的项。该项将把它自己从它的pxContainer
 * 指向的列表中移除
 * @return 返回移除该项后，列表中还有多少项
 *
 */
UBaseType_t uxListRemove( ListItem_t * const pxItemToRemove ) PRIVILEGED_FUNCTION;

#ifdef __cplusplus
}
#endif

#endif

