/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

	说明:
		这时演示了利用守护任务调用中断处理函数处理中断事件。
		这里我们要注意模拟产生中断事件的任务的优先级要低于守护任务的优先级	   
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

/* 示例包含头文件 */
#include "supporting_functions.h"


/* 本示例模拟中断号的使用。中断号0到2,用于FreeRTOS windows移植版系统使用。
所以第一个可以用的应用中断号是3 */
#define mainINTERRUPT_NUMBER	3

/* 要创建的任务 */
static void vPeriodicTask( void *pvParameters );

/* 这个函数执行中断延迟处理，该函数在上下文中的守护函数中被执行. */
static void vDeferredHandlingFunction( void *pvParameter1, uint32_t ulParameter2 );

/* 这个函数模拟中断服务函数。这就是那个任务要同步的中断。 */
static uint32_t ulExampleInterruptHandler( void );

/*-----------------------------------------------------------*/

int main( void )
{
/* 产生软中断的任务的优先级要低于守护任务的优先级。
守护任务的优先级是在FreeRTOSConfig.h这个文件中，配置的编译时常量
configTIMER_TASK_PRIORITY */
const UBaseType_t ulPeriodicTaskPriority = configTIMER_TASK_PRIORITY - 1;

	/* 创建一个周期性产生软中断的任务 */
	xTaskCreate( vPeriodicTask, "Periodic", 1000, NULL, ulPeriodicTaskPriority, NULL );

	/* 安装软中断处理函数句柄，这里的使用方式是基于FreeRTOS的移植实现。
		这种使用方式只是在FreeRTOS Windows上使用的，这里只是模拟中断  */
	vPortSetInterruptHandler( mainINTERRUPT_NUMBER, ulExampleInterruptHandler );

	/* 开启调度，使得任务被执行. */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vPeriodicTask( void *pvParameters )
{
const TickType_t xDelay500ms = pdMS_TO_TICKS( 500UL );

	/* 与大多数任务一样，本任务是一个无限循环  */
	for( ;; )
	{
		/* 该任务用于模拟一个中断，这里周期性的产生一个模拟的软中断。
		阻塞它直到再次产生软中断 */
		vTaskDelay( xDelay500ms );

		/* 产生一个中断，并在中断前后都输出一个消息，
		从消息的打印顺序来证明中断的执行顺序。

		这里产生软中断的方式与FreeRTOS的移植实现相关。
		这里的使用方式只适用于FreeRTOS的windows移植版本。
		这里只能是模拟产生中断. */
		vPrintString( "Periodic task - About to generate an interrupt.\r\n" );
		vPortGenerateSimulatedInterrupt( mainINTERRUPT_NUMBER );
		vPrintString( "Periodic task - Interrupt generated.\r\n\r\n\r\n" );
	}
}
/*-----------------------------------------------------------*/

static uint32_t ulExampleInterruptHandler( void )
{
static uint32_t ulParameterValue = 0;
BaseType_t xHigherPriorityTaskWoken;

	/* xHigherPriorityTaskWoken 这个参数开始要初始化为pdFALSE
	因为，如果要进行上下文切换，在中断的安全的API函数中，会将该变量置为pdTRUE. */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 把中断处理函数的指针发送给守护任务。
	中断处理函数的参数1没有使用，置为NULL,参数 2用于传递记录中断发生次数的变量。 */
	xTimerPendFunctionCallFromISR( vDeferredHandlingFunction, NULL, ulParameterValue, &xHigherPriorityTaskWoken );
	ulParameterValue++;

	/* 把xHigherPriorityTaskWoken的值传给portYIELD_FROM_ISR()函数。
	如果xHigherPriorityTaskWoken的值在xSemaphoreGiveFromISR()中被置为pdTRUE了。
	那么调用portYIELD_FROM_ISR()该函数，将会导致请求上下文切换。
	如果xHigherPriorityTaskWoken的值是pdFALSE,
	那么portYIELD_FROM_ISR()的调用将没有任务效果（即不产生任何影响）。

	在windows中，该函数的portYIELD_FROM_ISR()实现，包含返回语句。
	所以这里就不显式返回一个值了。
	（我们查看portYIELD_FROM_ISR()源码，实际就是一个return语句。）*/
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*-----------------------------------------------------------*/

static void vDeferredHandlingFunction( void *pvParameter1, uint32_t ulParameter2 )
{
	/* 这里主要是为了去除编译警告，因为这个变量在本例中没有使用*/
	( void ) pvParameter1;

	/* 处理事件 - 这里只是输出一个消息和参数 2的值 */
	vPrintStringAndNumber( "Handler function - Processing event ", ulParameter2 );
}










