/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    说明:
		创建两个低优先级的任务task1,task2,这两个任务
		没有阻塞，也没有延迟，就是连续打印任务名称。
		创建一个高优先级任务(Task 3)，每隔3ms精确执行一次。
		Task 3 是一个高优先级任务，所以当它的延迟时间结束，就能
		抢占cpu执行时间，并执行，当它进入延迟状态，其他两个低优先级
		任务task1,task2获得cpu,并执行.	
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 任务函数声明 */
void vContinuousProcessingTask( void *pvParameters );
void vPeriodicTask( void *pvParameters );

/* 定义字符串，将做为参数传递给任务函数.
这些值定义为常量，并且当任务正在执行时，
确保离开栈区，它们指定的值仍然有效。*/
const char *pcTextForTask1 = "Continuous task 1 running\r\n";
const char *pcTextForTask2 = "Continuous task 2 running\r\n";
const char *pcTextForPeriodicTask = "Periodic task is running\r\n";

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建两个连续处理的任务,它们的优先级都是1. */
	xTaskCreate( vContinuousProcessingTask, "Task 1", 1000, (void*)pcTextForTask1, 1, NULL );
	xTaskCreate( vContinuousProcessingTask, "Task 2", 1000, (void*)pcTextForTask2, 1, NULL );

	/* 创建一个优先级为 2的周期性任务. */
	xTaskCreate( vPeriodicTask, "Task 3", 1000, (void*)pcTextForPeriodicTask, 2, NULL );

	/* 开启调度，使得任务被执行 */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM.*/
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vContinuousProcessingTask( void *pvParameters )
{
char *pcTaskName;

	/* 将要被打印的字符串是通过参数传递过来的.  把它强制转换为字符串指针类型. */
	pcTaskName = ( char * ) pvParameters;

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 打印输出任务名字.  该任务只是重复输出和打印名字，没有阻塞和延迟*/
		vPrintString( pcTaskName );
	}
}
/*-----------------------------------------------------------*/

void vPeriodicTask( void *pvParameters )
{
TickType_t xLastWakeTime;
const TickType_t xDelay3ms = pdMS_TO_TICKS( 3UL );

	/* xLastWakeTime 需要用当前的 tick记数器初始化.  
	注意这里是我们唯一一次访问该变量。
	后续，xLastWakeTime将由vTaskDelayUntil()函数自动管理该变量的值*/
	xLastWakeTime = xTaskGetTickCount();

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 打印本任务的名字 */
		vPrintString( "Periodic task is running\r\n" );

		/* 希望该任务每3ms精确执行 */
		vTaskDelayUntil( &xLastWakeTime, xDelay3ms );
	}
}



