/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例演示notification的计数值，可以用于锁定通知事件。
		以便任务可以轮循处理任务。  
		与例24的区别，主要是ulTaskNotifyTake 第一个参数的值对notification的影响
*/

/* FreeRTOS.org 源文件头包含 . */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* 示例包含. */
#include "supporting_functions.h"

/* 本示例模拟中断号的使用。中断号0到2,用于FreeRTOS windows移植版系统使用。
所以第一个可以用的应用中断号是3 */
#define mainINTERRUPT_NUMBER	3

/* 要创建的任务. */
static void vHandlerTask( void *pvParameters );
static void vPeriodicTask( void *pvParameters );

/* 中断服务程序(模拟中断服务程序)。这就是任务将要同步的中断 */
static uint32_t ulExampleInterruptHandler( void );

/* 周期性任务产生软中断的频率. */
static const TickType_t xInterruptFrequency = pdMS_TO_TICKS( 500UL );

/* 用于保存延迟处理中断任务的句柄 */
static TaskHandle_t xHandlerTask = NULL;

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建handler（处理者）任务，这就是那个中断延迟处理任务，
	与中断同步的任务。该任务的创建优先级较高，以确保任务退出时，
	能立即运行。所以使用了优先级 3，保存的任务句柄将用于ISR中。 */
	xTaskCreate( vHandlerTask, "Handler", 1000, NULL, 3, &xHandlerTask );

	/* 这是一个周期性产生软中断的任务。该任务的优先级比处理任务handler的优
	先级低，是为了确保，当handler任务退出阻塞状态时，每次都能抢占cpu执行。*/
	xTaskCreate( vPeriodicTask, "Periodic", 1000, NULL, 1, NULL );

	/* 安装软中断处理函数句柄，这里的使用方式是基于FreeRTOS的移植实现。
		这种使用方式只是在FreeRTOS Windows上使用的，这里只是模拟中断 */
	vPortSetInterruptHandler( mainINTERRUPT_NUMBER, ulExampleInterruptHandler );

	/* 开启调度，执行任务. */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vHandlerTask( void *pvParameters )
{
/* xMaxExpectedBlockTime 的值被设置为比两个事件的间隔时间，稍长一点。
也就是说，该处理任务的阻塞时间，比软中断产生的时间要长一点。
这样就一定能，在退出阻塞前捕捉到中断的产生。 */
const TickType_t xMaxExpectedBlockTime = xInterruptFrequency + pdMS_TO_TICKS( 10 );

	/* 与大多数作任务一样，该任务是无限循环. */
	for( ;; )
	{
		/* 等待接收从中断处理函数中直接发来的通知。
		xClearCountOnExit 的值现在是pdFALSE,
		所以当ulTaskNotifyTake接收到一个通知返回时，
		任务的通知计数将会减少1。（这里要和例24对照起来看）
		通过查看源码
		如果xClearCountOnExit为pdTRUE.任务的通知计数器直接=0.*/
		if( ulTaskNotifyTake( pdFALSE, xMaxExpectedBlockTime ) != 0 )
		{
			/* 执行到这里时事件肯定已经发生，处理这个事件
			(这里的处理指的是打印每个任务对应的消息) */
			vPrintString( "Handler task - Processing event.\r\n" );
		}
		else
		{
			/* 如果代码运行到这里了，就说明中断没有在期望的时间内发生，
			并且（在真实的应用场景中）有秘要进行一些错误恢复操作。 */
		}
	}
}
/*-----------------------------------------------------------*/

static uint32_t ulExampleInterruptHandler( void )
{
BaseType_t xHigherPriorityTaskWoken;

	/*  xHigherPriorityTaskWoken 这个参数开始要初始化为pdFALSE
	因为，如果要进行上下文切换，在中断的安全的API函数中，会将该变量置为pdTRUE.  */
	xHigherPriorityTaskWoken = pdFALSE;

	/* 给任务处理函数发送多次通知消息。第一个消息用于解除任务的阻塞状态。
	后面多次'gives'消息，主要演示接收任务的通知值，可用于锁定事件--
	以便作务可以轮循处理发生的事件。*/
	vTaskNotifyGiveFromISR( xHandlerTask, &xHigherPriorityTaskWoken );
	vTaskNotifyGiveFromISR( xHandlerTask, &xHigherPriorityTaskWoken );
	vTaskNotifyGiveFromISR( xHandlerTask, &xHigherPriorityTaskWoken );

	/* 把xHigherPriorityTaskWoken的值传给portYIELD_FROM_ISR()函数。
	如果xHigherPriorityTaskWoken的值在vTaskNotifyGiveFromISR()中被置为pdTRUE了。
	那么调用portYIELD_FROM_ISR()该函数，将会导致请求上下文切换。
	如果xHigherPriorityTaskWoken的值是pdFALSE,
	那么portYIELD_FROM_ISR()的调用将没有任务效果（即不产生任何影响）。

	在windows中，该函数的portYIELD_FROM_ISR()实现，包含返回语句。
	所以这里就不显式返回一个值了。
	（我们查看portYIELD_FROM_ISR()源码，实际就是一个return语句。）  */
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
/*-----------------------------------------------------------*/

static void vPeriodicTask( void *pvParameters )
{
	/* 同大多数任务一样，该任务是一个无限循环. */
	for( ;; )
	{
		/* 该任务用于模拟一个中断，这里周期性的产生一个模拟的软中断。
		阻塞它直到再次产生软中断. */
		vTaskDelay( xInterruptFrequency );

		/* 产生一个中断，并在中断前后都输出一个消息，
		从消息的打印顺序来证明中断的执行顺序。

		这里产生软中断的方式与FreeRTOS的移植实现相关。
		这里的使用方式只适用于FreeRTOS的windows移植版本。
		这里只能是模拟产生中断. */
		vPrintString( "Periodic task - About to generate an interrupt.\r\n" );
		vPortGenerateSimulatedInterrupt( mainINTERRUPT_NUMBER );
		vPrintString( "Periodic task - Interrupt generated.\r\n\r\n\r\n" );
	}
}









