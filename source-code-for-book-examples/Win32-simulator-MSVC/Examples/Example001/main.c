/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved
	说明:简单演示FreeRTOS的调度
		创建两个任务，在两个任务中打印不同的字符串，表示自己被调度运行了
    1 tab == 4 spaces! 
	代码缩进:
	1个tab键 == 4空格!
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 使用循环计数来实现延时功能 */
#define mainDELAY_LOOP_COUNT		( 0xffffff )

/* 任务函数声明 */
void vTask1( void *pvParameters );
void vTask2( void *pvParameters );

/*-----------------------------------------------------------*/

int main( void )
{
	/* 创建其中一个任务 */
	xTaskCreate(	vTask1,		/* 指向任务的实现函数 */
					"Task 1",	/* 任务名称，这个名称方便调试用 */
					1000,		/* 栈深度 -大多数微处理器的栈都比这个值要小 */
					NULL,		/* 这里不打算给任务传递参数 */
					1,			/* 本任务的运行优先级 1 */
					NULL );		/* 这里不打算用任务句柄 */

	/* 使用同样的方式创建另一个任务，只是换了个名称 */
	xTaskCreate( vTask2, "Task 2", 1000, NULL, 1, NULL );

	/* 开启调度，使得任务被执行 */
	vTaskStartScheduler();	

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM.*/
	
	/* 对于搞嵌入式的开发人员来说，一定得搞清楚RAM，ROM 这两个概念。
	RAM:指是随机访问内存，访问速度快，数据掉电丢失。与cpu交换数据用，硬件实物就是ddr或其他 sdram芯片
	ROM:指的是只读访问内存，用于存放代码。平常所说的刷机，实际是把程序烧录到这个ROM中。
	*/
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTask1( void *pvParameters )
{
const char *pcTaskName = "Task 1 is running\r\n";
volatile uint32_t ul;

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 打印本任务的名字 */
		vPrintString( pcTaskName );

		/* 延迟一段时间 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环是一种暴力(不精确的)延时.  这里什么也不做，
			后面的代码我们会使用delay或sleep 函数来替换这个循环 */
		}
	}
}
/*-----------------------------------------------------------*/

void vTask2( void *pvParameters )
{
const char *pcTaskName = "Task 2 is running\r\n";
volatile uint32_t ul;

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 打印本任务的名字 */
		vPrintString( pcTaskName );

		/* 延迟一段时间 */
		for( ul = 0; ul < mainDELAY_LOOP_COUNT; ul++ )
		{
			/* 这个循环是一种暴力(不精确的)延时.  这里什么也不做，
			后面的代码我们会使用delay或sleep 函数来替换这个循环 */
		}
	}
}


