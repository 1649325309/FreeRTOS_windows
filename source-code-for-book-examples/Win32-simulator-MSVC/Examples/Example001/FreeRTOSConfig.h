/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
*/


#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * 应用相关的定义
 *
 * 这些定义需要根据硬件和应用的要求来做调整
 * 
 * 关于这些参数的作用在
 *	https://www.freertos.org/a00110.html
 *  有详细的描述,
 *  译者注: 在这里把这些描述放到该文件的最后面，方便查看
 *----------------------------------------------------------*/

#define configUSE_PREEMPTION						1 
#define configUSE_PORT_OPTIMISED_TASK_SELECTION	1 
#define configMAX_PRIORITIES						5
#define configUSE_IDLE_HOOK						0
#define configUSE_TICK_HOOK						0
#define configTICK_RATE_HZ						( 100 ) /* 这个是模拟环境，不是实时时间 */
#define configMINIMAL_STACK_SIZE				( ( unsigned short ) 50 ) /* 在这个模拟的例子中，堆栈只需要包含一个小结构，因为实际堆栈是win32线程的一部分. */
#define configTOTAL_HEAP_SIZE					( ( size_t ) ( 20 * 1024 ) )
#define configMAX_TASK_NAME_LEN					( 12 )
#define configUSE_TRACE_FACILITY				0
#define configUSE_16_BIT_TICKS					0
#define configIDLE_SHOULD_YIELD					1
#define configUSE_MUTEXES						1
#define configCHECK_FOR_STACK_OVERFLOW			0 /* Win32 模拟仿真不可用. */
#define configUSE_RECURSIVE_MUTEXES				1
#define configQUEUE_REGISTRY_SIZE				10
#define configUSE_MALLOC_FAILED_HOOK			1
#define configUSE_APPLICATION_TASK_TAG			0
#define configUSE_COUNTING_SEMAPHORES			1
#define configUSE_ALTERNATIVE_API				0
#define configUSE_QUEUE_SETS					1

/* 软定时器相关的配置选项 */
#define configUSE_TIMERS						0
#define configTIMER_TASK_PRIORITY				( configMAX_PRIORITIES - 1 )
#define configTIMER_QUEUE_LENGTH				20
#define configTIMER_TASK_STACK_DEPTH			( configMINIMAL_STACK_SIZE * 2 )

/* 运行时状态收集配置项 */
#define configGENERATE_RUN_TIME_STATS			0

/* Co-routine 相关配置项 */
#define configUSE_CO_ROUTINES 					0
#define configMAX_CO_ROUTINE_PRIORITIES 		2

/* 这个示例没有使用任务格式化函数，所谓格式化函数就是把收
 * 集的系统状态的原始数据，转换成可阅读的ASCII格函的函数
 * 即uxTaskGetSystemState()
*/
#define configUSE_STATS_FORMATTING_FUNCTIONS	0

/* 将下面的定时设置为1,将包含相关API函数，或者为0
 * 将不包含相关函数.不管怎样，(大多数情况下链接器将
 * 会移除没有用的函数)
 * 译注:
 * 括号里这句话，是说，即使我们在这里设置为1了，但是
 * 我们并没有调用相关函数，那么在多数情况下，链接器
 * 在链接时，并不会把相关的代码链接到最终执行的程序中
 * 去。这是一个编译器链接相关的知识点。如果不用相关代码
 * 生成最终可执行代码时，并不会包括没有用到的代码。
 */
#define INCLUDE_vTaskPrioritySet				1
#define INCLUDE_uxTaskPriorityGet				1
#define INCLUDE_vTaskDelete						1
#define INCLUDE_vTaskSuspend					1
#define INCLUDE_vTaskDelayUntil					1
#define INCLUDE_vTaskDelay						1
#define INCLUDE_uxTaskGetStackHighWaterMark		1
#define INCLUDE_xTaskGetSchedulerState			1
#define INCLUDE_xTimerGetTimerDaemonTaskHandle	1
#define INCLUDE_xTaskGetIdleTaskHandle			1
#define INCLUDE_pcTaskGetTaskName				1
#define INCLUDE_eTaskGetState					1
#define INCLUDE_xSemaphoreGetMutexHolder		1
#define INCLUDE_xTimerPendFunctionCall			1

/* 在开发时定义configASSERT() 宏非常有用。这个宏与标准 c的
 * assert()函数的语义是一样的.
 */
extern void vAssertCalled( uint32_t ulLine, const char * const pcFileName );
#define configASSERT( x ) if( ( x ) == 0 ) vAssertCalled( __LINE__, __FILE__ )


/*-----------------------------------------------------------
*	configUSE_PREEMPTION
*	1:表示使用抢占式调度算法 
*   0:表示使用协作式调度算法
*-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 * configUSE_PORT_OPTIMISED_TASK_SELECTION
 * 有些FreeRTOS的移植有两种选择下个可执行任务的方式
 * 通用方式和特定的移植方式
 * 通用方式:
 *	0:表示选择通用方法选择下个执行的任务或者移植没有实现时用
 *  可用于所有FreeRTOS的移植
 *  是用C来实现的，相对于特定的移植来说效率较低
 *  不限制最大优先级的值
 *  特定的移植方式: 
 *   1: 表示根据芯片用相应的汇编指定实现
 *	 只对特定的移植适用(只适用于某种平台或芯片)
 *   依赖于某个或多个架构的汇编指令(一般是(CLZ)或类似功能指令)
 *	 所以只能用于适专门为其编写的体系架构
 *	 比通用方式更高效
 *   一般限制最大可用优先级为32
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configMAX_PRIORITIES
 *	应用任务可使用的优先级。任务(任意多)可使用相同的优先级
 *  Co-routines(协作模式)的优先级是独立的
 *  可以参考configMAX_CO_ROUTINE_PRIORITIES
 *	每一个可用的优先级都会在RTOS内核中消耗少量的内存，所以这个
 *  值不应设置得比你的应用实际需要的值高太多
 *		
 *	configUSE_PORT_OPTIMISED_TASK_SELECTION 值为1时，
 *	优先级的最大将受到限制(受特定体系架构的限制)
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configUSE_IDLE_HOOK
 *	1:给空闲任务设置一个钩子函数，置1我们需要定一个钩子函数
 *	  空闲任务运行时会调用这个钩子函数,
 *    查找这个宏就能找到函数声明,及调用
 *	  extern void vApplicationIdleHook( void );
 *  0:不给空闲任务设置钩子函数,因此也不需要实现
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configUSE_TICK_HOOK
 *	1:表示要用一个tick_hook函数，自己实现一个函数
 *  0:表不需要用TICK_HOOK函数
 *   extern void vApplicationTickHook( void );
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configTICK_RATE_HZ
 *	RTOS tick 中断频率
 *	计时中断用于测量时间。因此，更高的tick(节拍)频率意味着时间
 *  可以被测量到更高的分辨率。但时，高频率也意味着RTOS内核将占
 *	更多的CPU时间，所以效率就更低了。RTOS所有的示例应用的值都是
 *  1000HZ.这个值是用来测试RTOS内核的，可能比实际需要使用的频率高
 *  译注:
 *  (值越大，在相同的时间内产生的中断次数就越多，
 *   任务调度就更频繁,调度越频繁，那么调度就会占更多的CPU时间
 *	 这样CPU用在实际任务上的时间就相应的减少了，CPU的使用效率真就
 *	 更低，这里的效率指的是CPU运行实际任务与调度任务所花费的时间比)
 *	
 *	另外就是多个任务可以共享同一个优先级。RTOS内核将会在每一个
 *  RTOS的tick周期,通过让相同优先级的每个任务,轮流使用处理器时间。
 *	这样也就表示频率越高，每个任务的时间片也就越少。
 *	译注:
 *  (时间片概念:
 *	这个tick频率与芯片的CPU频率是什么关系?
 *	假如一个CPU的频率是100MHZ,这个的tcik_RATE_HZ 是1000.
 *	那么多少个 tick产生一次中断呢?
 *	计算方式为:  100MHZ / 1KHZ = 100KHZ  换算成时间即:100ms中断一次
 *	如果系统中只有三个任务ABC,它们的优先级是一样的，
 *	那么这三个任务平分100ms,每个任务约33.33ms.这个概念就是时间片
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configMINIMAL_STACK_SIZE
 *	这个值是空闲任务的栈的大小，一般我们不需要把在FreeRTOSConfig.h
 *  中定义的这个值减少。
 *  与 xTaskCreate() and xTaskCreateStatic()这两个函数一样
 *  栈的单位是字(2个bytes)
 *  举例:
 *  如果栈中每一项是32-bits(4bytes).那么放100项，就需要400字节
 *  那么相应的栈的大小就 400/2= 200,也就是说该宏的值就是200
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configTOTAL_HEAP_SIZE
 *	FreeRTOS堆空间，即RAM总大小。
 *  这个值只有在configSUPPORT_DYNAMIC_ALLOCATION 的值为1
 *	并且使用FreeRTOS源码中的某个内存分存方式的代码时才起作用。
 *	详情请查看内存配置管理相关部分
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configMAX_TASK_NAME_LEN
 *	这个值规定了任务创建时，任务名称参数的最大长度
 *  包括了字符串结束符所占的字节。
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configUSE_TRACE_FACILITY
 *	设置为1,将会包含更多的结构成员和辅助函数
 *  用于跟踪调度代码用。在移植内核和调试应用时可以试试这个值
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 *	configUSE_16_BIT_TICKS
 *	FreeRTOS中时间是以tick做为度量单位的---这个值是指RTOS内核
 *  启动后有多少个tick 中断被执行了，这个值保存在一个TickType_t
 *	类型的变量中。
 *	该值置为1,将会使用TickType_t的值被定义为一个无符号16bit类型
 *	该值置为0,将会使用TickType_t的值被定义为一个无符号32bit类型
 *
 *	在8或16位的芯片中，定义为16bit的类型，将能极大的提升性能。
 *	但同时也制约了最大的可用时间周期为65535'ticks'.
 *	(即16bit所能表达的最大值:2的16次方减去1)
 *	因此，所如tick的频率是250hz,则一个任务可以延迟和阻塞的最大
 *	时间是262秒，而32bit最大可以延迟或阻塞17179869秒.
 *	这个是怎么算的?
 *	65535 / 250Hz  = 262秒  
 *	4294967295 / 250Hz = 17179869秒 
 *  (这里计算除出来还有余数，因为余数不够一秒，所以略掉)
 *-----------------------------------------------------------*/


/*----------------------------------------------------------- 
 *	configIDLE_SHOULD_YIELD
 *	这个值影响空闲优先级任务的表现，只有满足如下两个条件才有效
 *  1:FreeRTOS使用了抢占式调度
 *	2:任务运行在空闲优先级
 *	
 *	如果configUSE_TIME_SLICING被置为1(或没有定义)，那么在相同
 *  优先级的任务将共享时间。如果这些在相同优先级的任务没有被
 *	其他任务抢占，那么给定优先级的任务将会得到相同的处理时间--
 *	如果任务的优先级高于空闲任务的优先级也是一样的(同理)
 *	
 *	当任务共享了空闲任务的优先级时，情况会稍有不同.如果
 *	configIDLE_SHOULD_YIELD 的值为1,那么其他使用空闲任务优先
 *	级的任务准备就绪时，空闲任务将立即yield,让出cpu时间.这将确保
 *	在其他任务的就绪可以调度时，空闲任务尽量少的 占用cpu 时间。
 *	如下图所示这种状态，产生所不期望的影响(这个与具体的应用要求相关)
 *  T0       T1       T2       T3       T4       T5       T6
 *	-------- -------- -------- -------- -------- -------- --------
 *	| B    | | C    | |I| A  | | B    | | C    | |I|A   | | B    |
 *	-------- -------- -------- -------- -------- -------- --------
 *	上图展示了4个处于空闲优先级任务的运行情况。任务ABC是应用任务，
 *	任务I是空闲任务。在时间T0,T1,...T6这几个时间段上，产生上下文切换。
 *	当空闲任务让出cpu给任务A执行时--空闲任务仍然消耗了一点时间片.
 *	这将导至任务I与任务A使用了同一个时间片。那么任务B,C将会比A就获得了
 *	更多的处理时间。
 *
 *	上面这种情况，可以通过如下方式避免:
 *	1:如果合适，使用空闲钩子代替空闲优先级的单独任务。
 *	2:应用创建的所有任务的优先级比空闲任务的优先级高
 *	3:configIDLE_SHOULD_YIELD的值设为0
 *
 *	设置configIDLE_SHOULD_YIELD为0时，将防止idle任务在其时间片
 *	用完之前，不会让出cpu处理时间.这样就确保了所有的处理空闲优先级的任务
 *	都得到相同的处理时间(如果这此任务没有被抢占)--代价就是空闲任务获得了
 *	更多的处理时间.
 *	译注:
 *		上图的示例在configIDLE_SHOULD_YIELD 为0 时，变成如下
 *  T0       T1       T2       T3       T4       T5       T6
 *	-------- -------- -------- -------- -------- -------- --------
 *	| B    | | C    | |I     | | A    | | B    | |C     | | I    |
 *	-------- -------- -------- -------- -------- -------- --------
 *  每个任务独占一个整个时间
 *
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 * configUSE_MUTEXES
 *
 * 1:设置为1将会把mutex相关的函数编译进来
 * 0:设置为0将不把mutex相关的函数编译
 * 读者需要自己去熟悉互斥量(mutexes)与二值信号量(binary semaphores)
 * 在FreeRTOS中的功能与区别
 *-----------------------------------------------------------*/

/*----------------------------------------------------------- 
 * configCHECK_FOR_STACK_OVERFLOW
 * 0:表示不会使用栈溢出查功能
 * 1:表示使用栈溢出检查功能,并使用方法1
 * 2:表示使用栈溢出检查功能,并使用方法2
 * 在windows移植的版本是win32线程的栈，所以这个检查不可用
 * https://www.freertos.org/Stacks-and-stack-overflow-checking.html
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_RECURSIVE_MUTEXES
 * 1:表示编译时包含递归互斥相关函数
 * 0:表示编译时不包含递归互斥相关函数 
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configQUEUE_REGISTRY_SIZE
 * 这个值有两个作用，都是跟感知内核调试有关系
 * <1>把名称与队列关联起来，在调试时可以通过名称知道是那个队列
 * <2>它包含调试器定位每个已注册队列和信号量所需的信息
 * 这个值只有在调试内核信息时才有作用，如果不需要感知内核调试
 * 它没有作用.
 *  
 * 这个值定义了最大可以注册多少个队列和信号量.
 * 只需要注册那些您想使用RTOS内核感知调试器查看的队列和信号量。
 * 具体详情可以看如下两个函数
 *  vQueueAddToRegistry() and vQueueUnregisterQueue() 
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_MALLOC_FAILED_HOOK
 * 每次创建一个task,queue,或semaphore 内核都会调用pvPortMalloc() 
 * 从堆里面申请内存。
 * 从官方下载的RTOS中包括了4个内存分配方案的示例代码。这4个方案
 * 分别在 heap_1.c, heap_2.c, heap_3.c, heap_4.c 和 heap_5.c 
 * 4个源码中实现.
 * configUSE_MALLOC_FAILED_HOOK 只有当上面的某一种分配方案使用时
 * 这个配置才是有效的。
 * 当pvPortMalloc()返回空时，如果定义了这个内存失败的钩子函数，
 * 并且该宏配置为1时，就会被调用。当RTOS中剩余的内存不够分配时
 * 就会返回NULL.
 * 
 * configUSE_MALLOC_FAILED_HOOK 为1时，应用必需定义一个内存分配
 * 失败的钩子函数，如果 configUSE_MALLOC_FAILED_HOOK 为0，即使定
 * 义了这个钩子函数，内存分失败时，还是不会调的。
 * 这个钩子函数的名字和原型必须是：
 * void vApplicationMallocFailedHook( void ); 
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_APPLICATION_TASK_TAG
 * 官网上没有这个说明
 * 从代码的查看，主要是提供了几个函数可以给每个任务设置
 * 一个钩子函数，获取任务的钩子函数，调用任务的钩子函数
 * 主要是如下三个函数，这个配置可根据需要来自己设置
 * 	void vTaskSetApplicationTaskTag( TaskHandle_t xTask, TaskHookFunction_t pxHookFunction )
 *  TaskHookFunction_t xTaskGetApplicationTaskTag( TaskHandle_t xTask )
 *  BaseType_t xTaskCallApplicationTaskHook( TaskHandle_t xTask, void *pvParameter )
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_COUNTING_SEMAPHORES
 * 1：表示包含计数信号量(编译时包含相关代码)
 * 0: 表示不包含计数信号量(编译时不包含相关代码)
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_ALTERNATIVE_API
 * 1：表示包含可选队列(编译时包含相关代码)
 * 0: 表示不包含可选队列(编译时不包含相关代码)
 * 在新的代码版本中，建意不要包含，即不用这个代码了
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_QUEUE_SETS
 * 1：表示包含队列集(set)(编译时包含相关代码)
 * 0: 表示不包含队列集(set)(编译时不包含相关代码)
 * queue_sets 相关的函数，可以同时挂起，阻塞多个队列信号量
 * https://www.freertos.org/Pend-on-multiple-rtos-objects.html
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configUSE_TIMERS
 * 1：表示使用定时器功能
 * 0: 表示不使用定时器功能
 * 有几个特点：
 * 1>不是中断上下文中来调用定时器回调函数的
 * 2>除非定时器时间到期，平时不占用任务处理时间
 * 3>不会给tick中断增加任何开销
 * 4>在中断禁止时不遍历任何链表结构
 * https://www.freertos.org/RTOS-software-timer.html
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configTIMER_TASK_PRIORITY
 * 设置定时器(服务/守护)任务的优先级
 * 这里的值是任务的最大优先级减1
 * https://www.freertos.org/RTOS-software-timer.html
 *-----------------------------------------------------------*/

/*-----------------------------------------------------------
 * configTIMER_QUEUE_LENGTH
 * 软定时器命令队列深度
 * 这里需要了解一下定时器守护任务与定时器命令队列
 * 首先，软定时器功能不是FreeRTOS内核的核心功能。它是用定时器守
 * 护任务来实现的。我们在调用定时器相关函数时，实际上是向定时器
 * 守护任务的命令队列发送操作定时器相关的命令。这样就引出了另一个
 * 问题，这个命令队列最大同时装多少个这样的命令？这样又引出另一个
 * 问题，调用定时器开始，停止等相关函数时，这些命令不是立即执行了吗？
 * 那么我们就需要找出什么情况下命令不会被立即执行:
 * 1>在RTOS开始调度之前(也就是说在定时器服务任务创建之前)
 *   多次调用了定时器相关的函数
 * 2>在中断服务函数，多次调用了中断安全的定时器函数
 * 3>在比定时器任务优先级高的任务中，多次调用了定时器相关函数
 * 在这些情况下调用时，命令不会立即执行。就会一直堆在命令队列中
 * https://www.freertos.org/RTOS-software-timer-service-daemon-task.html
 *-----------------------------------------------------------*/



#endif /* FREERTOS_CONFIG_H */



